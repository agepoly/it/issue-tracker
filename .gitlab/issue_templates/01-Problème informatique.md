<!-- Ceci est un commentaire, il ne sera pas affiché lors de la demande -->

<!-- Avant de continuer, prière de suivre les instructions suivantes à fin de mieux pouvoir adresser votre inconvenant. -->
<!-- 1. Vérifiez que le problème n'as pas déjà été réporté. -->
<!-- 2. Réflechisez à un titre bref, descriptif et précis. -->
<!-- 3. Écrivez des descriptions claires et non-ambigues. -->
<!-- 4. Ajoutez des captures d'écran si possible à fin de mieux comprendre le problème. -->
<!-- 5. Relisez et vérifiez l'issue avant de transmette. -->

# Problème informatique
- Description:
 <!-- Vous avez de la place, exprimez vous ! -->
- Comportement attendu:
 <!-- Description de ce que vous attendiez voir -->
- Comportement observé:
 <!-- Description de ce que vous avez vu -->
- Étapes pour le réproduire:
    -   <!-- Prìère de lister étape par étape -->


__Merci de votre demande, elle sera traité dans les plus bref délais. Thanks for flying AGEPoly__

<!-- Fin des informations du requérant. Prière de ne pas toucher aux lignes ci-dessous. -->
/label ~"@à-faire"
/label ~"priority/3:required"
/label ~"type:problème"
/assign @responsable\_informatique\_agepoly