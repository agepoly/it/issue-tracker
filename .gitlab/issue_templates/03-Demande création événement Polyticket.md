# Demande de création d'un événement sur Polyticket
<!-- Ceci est un commentaire, il ne sera pas affiché lors de la demande -->
## À remplir par le requérant

### Informations de base

* Nom: 
* Adresse contact: `@epfl.ch`
* Téléphone: 

<!-- Si vous souhaitez garder cet événement confidentiel, veuillez cocher la case “This issue is confidential...” juste en bas de cette boîte -->

#### Titre du Polyticket
<!-- Veuillez ajouter le titre de l'événement ici -->

#### Description du Polyticket
<!-- Veuillez ajouter la description de l'événement entre les symboles `>>>`. Vous avez de la place, donc exprimez vous ! -->
>>>
Bienvenu sur mon évenement
Viens te réjouir avec nous !
>>>

<!-- Prière de remplir tous les champs ci-dessous de façon brève et précise. Vous pourrez ajouter plus d'informations plus en bas -->
* Date souhaité de mise en test: 
* Date souhaité de mise en production: 
* Date de fin du Polyticket: 
* Numéro total de places disponible: 
* Prix des places: 
* Ajouter les frais de traitement de polyticket au prix des places (Oui/Non): 
* Étudiants EPFL uniquement (Oui/Non): 

### Informations complémentaires
Si vous avez d'autres demandes (e.g. Plusieurs places par étudiant, goodie bags, rabais) veuillez le préciser ci-dessous:
<!-- Vous êtes libre d'ajouter ici ce qui vous estimez convenable -->

### Informations avancés
> Si vous ne savez pas quoi répondre, laissez-le vide

Questions supplémentaires à demander au moment d'inscription:

- Event Path: 
- Payement aussi possible à la boutique de l'AGEPoly (Chez Marianne) (Oui/Non): 
- Limites: 
- Plusieurs comptes par adresse email: 
- Plusieurs billets par compte: 

__Merci de votre demande, elle sera traité dans les plus bref délais. Thanks for flying AGEPoly__

# ~~ Fin des informations du requérant. Prière de ne pas toucher aux lignes ci-dessous. ~~

/label ~"@à-faire"
/label ~"priority/3:required"
/label ~"type:tâche"
/assign @responsable\_informatique\_agepoly

## Responsable Informatique
* [ ] Configuration de l'événement
* [ ] Configuration du profile de l'utilisateur
* [ ] Configuration des Dates
* [ ] Configuration des Prix
* [ ] Configuration des Limites

### __Étape Test__
Polybanking __Test__:
* [ ] Renouvèlement des Tokens
* [ ] Configurations des URL (IPN, OK, ERR)

Module Polybanking sur Polyticket:

* [ ] Configuration ID
* [ ] Configurations des Tokens (Requests, IPN, API)

Wrapup
* [ ] Mise en mode test
* [ ] Feedback du responsable

### __Étape Production__
Polybanking __Production__:
* [ ] Renouvèlement des Tokens
* [ ] Configurations des URL (IPN, OK, ERR)

Module Polybanking sur Polyticket:
* [ ] Configuration ID
* [ ] Configurations des Tokens (Requests, IPN, API)

Wrapup
* [ ] Mise en mode production
* [ ] Feedback du responsable
